<?php
defined('TYPO3_MODE') or die();


$additionalColumnsTtcontent = [
    'element_id' => [
        'exclude' => true,
        'label' => 'LLL:EXT:kz_ce_extend/Resources/Private/Language/locallang.xml:element_id',
        'config' => [
            'type' => 'input',
			'size' => '50',
			'eval' => 'trim'
        ]
    ],
    'element_custom_classes' => [
        'exclude' => true,
        'label' => 'LLL:EXT:kz_ce_extend/Resources/Private/Language/locallang.xml:element_custom_classes',
        'config' => [
            'type' => 'input',
			'size' => '50',
			'eval' => 'trim'
        ]
    ],
    'header_custom_classes' => [
        'exclude' => true,
        'label' => 'LLL:EXT:kz_ce_extend/Resources/Private/Language/locallang.xml:header_custom_classes',
        'config' => [
            'type' => 'input',
			'size' => '50',
			'eval' => 'trim'
        ]
    ],
    'element_htmldir' => [
        'exclude' => true,
        'label' => 'LLL:EXT:kz_ce_extend/Resources/Private/Language/locallang.xml:element_htmldir',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
				['Auto', 0],
				['Left to right (ltr)', 1],
				['Right to left (rtl)', 2],
			],
            'default' => 0
        ]
    ],
    'image_gutters' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:kz_ce_extend/Resources/Private/Language/locallang.xml:image_gutters',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
				[ 'Normal', 0 ],
				[ 'Big', 1 ],
				[ 'Thin', 2 ],
				[ 'None', 3 ],
			],
            'default' => 0
        ]
    ]
];

$additionalColumnsSysFileReference = [
    'image_classes' => [
        'exclude' => true,
        'label' => 'Image Classes',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectCheckBox',
            'itemsProcFunc' => 'Kabaz\KzCeExtend\UserFunction\ProviderField->createSysFileClassesItems',
            'items' => [],
            'multiple' => true,
            'minitems' => 0,
            'maxitems' => 999
        ]
    ]
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $additionalColumnsTtcontent);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', 'header_custom_classes', '', 'after:header_link');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', 'element_id', '', 'after:layout');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', 'element_custom_classes', '', 'after:layout');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', 'element_htmldir', '', 'after:element_id');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', 'image_gutters', '', 'after:imageorient');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file_reference', $additionalColumnsSysFileReference);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_file_reference','image_classes');
$GLOBALS['TCA']['sys_file_reference']['palettes']['imageoverlayPalette']['showitem'] .=",--linebreak--,image_classes";


?>