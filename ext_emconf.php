<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "kz_ce_extend"
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'CE Extend',
    'description' => 'Extend Content-Elements with new fields',
    'category' => 'misc',
    'author' => 'Kai Vier',
    'author_email' => 'info@kabaz.de',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.1.2',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.0-9.5.99'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
