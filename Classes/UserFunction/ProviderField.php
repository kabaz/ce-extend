<?php

namespace Kabaz\KzCeExtend\UserFunction;

use WapplerSystems\WsT3bootstrap\Service\ConfigurationService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;


class ProviderField extends \WapplerSystems\WsT3bootstrap\UserFunction\ProviderField
{
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
    }

    public function createSysFileClassesItems($config) {
        $settings = $this->configurationService->getSettingsForExtensionName('kz_ce_extend');
        $classNames = json_decode($settings['image']['classNames'],true);
        if (!is_array($classNames)) return $config;

        $optionList = array();
        foreach ($classNames as $key => $val) {
            $optionList[] = [$val, $key];
        }

        $config['items'] = array_merge($config['items'], $optionList);
        return $config;
    }

}