
#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
	element_custom_classes varchar(255) DEFAULT '' NOT NULL,
	element_id varchar(255) DEFAULT '' NOT NULL,
	element_htmldir tinyint(4) DEFAULT NULL,
	header_custom_classes varchar(255) DEFAULT '' NOT NULL,
	image_gutters tinyint(4) DEFAULT NULL
);

CREATE TABLE sys_file_reference (
	image_classes  VARCHAR(255) DEFAULT '' NOT NULL
);
