<?php
defined('TYPO3_MODE') || die('Access denied.');


(function($_EXTKEY) {
          
		//# Add page TSConfig
		$pageTsConfig = \TYPO3\CMS\Core\Utility\GeneralUtility::getUrl(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TsConfig/Page/config.ts');
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig($pageTsConfig);       
        
})('kz_ce_extend');

